﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Our.Umbraco.Vorto.Extensions;

namespace A8.Catalogues
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Type { get; set; }
        public A8.DateFromTo DateFromTo { get; set; }
        public string Summary { get; set; }
        public IEnumerable<IPublishedContent> Content { get; set; }
        public string Location { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public bool ExludeFromList { get; set; }
        public int Priority { get; set; }

        public Event(
            int id,
            string name,
            string image,
            string type,
            A8.DateFromTo dateFromTo,
            string summary,
            IEnumerable<IPublishedContent> content,
            string location,
            string keywords,
            string description,
            bool excludeFromList,
            int priority
            )
        {
            this.Id = id;
            this.Name = name;
            this.Image = image;
            this.Type = type;
            this.DateFromTo = dateFromTo;
            this.Summary = summary;
            this.Content = content;
            this.Location = location;
            this.Keywords = keywords;
            this.Description = description;
            this.ExludeFromList = excludeFromList;
            this.Priority = priority;
        }
    }

    public static class Events
    {
        /// <summary>
        /// Return items as IpublishContent
        /// </summary>
        public static List<IPublishedContent> LoadAllIEvents(EventsFilter filter)
        {
            return _LoadAllEvents(filter);
        }

        /// <summary>
        /// Return items as Events
        /// </summary>
        public static List<Event> LoadAllEvents(EventsFilter filter)
        {
            return LoadEvents(_LoadAllEvents(filter), filter);
        }

        private static List<IPublishedContent> _LoadAllEvents(EventsFilter filter)
        {
            List<IPublishedContent> lstItems = new List<IPublishedContent>();

            int rootId = Array.ConvertAll(A8.CommonUmbraco.GetCurrentContent().Path.Split(','), int.Parse)[1];
            IEnumerable<IPublishedContent> items = UmbracoContext.Current.ContentCache.GetByXPath("id(" + rootId + ")[@isDoc]/ancestor-or-self::site[@isDoc]/cat_Catalogues[@isDoc]/cat_Events[@isDoc]/cat_Event[@isDoc]").OfType<IPublishedContent>().Where(x => x.IsVisible()).ToList();

            if (items.Any() && filter != null && filter.EventType != null && filter.EventType.Length > 0)
            {
                items = items.Where(x => ((string)x.GetPropertyValue<string>("catalogueType") == filter.EventType)).ToList();
            }

            //if (items.Any() && filter != null && filter.EventType != null && filter.EventType.Any())
            //{
            //    items = items.Where(x => ((string[])x.GetPropertyValue<string>("catalogueType").Split(',')).Intersect(filter.EventType).Any());
            //}

            if (items.Any())
            {
                lstItems = items.ToList();
            }

            return lstItems;
        }

        public static Event LoadEvent(IPublishedContent content)
        {
            List<IPublishedContent> contents = new List<IPublishedContent>();
            if (content != null)
            {
                contents.Add(content);
            }
            return LoadEvents(contents, null).FirstOrDefault();
        }

        public static List<Event> LoadPickerEvents(IPublishedContent content, string pickerPropertyAlias)
        {
            List<IPublishedContent> lstItems = new List<IPublishedContent>();

            var items = content.GetPropertyValue<IEnumerable<IPublishedContent>>(pickerPropertyAlias);
            if (items != null && items.Any())
            {
                lstItems = items.ToList();
            }

            return LoadEvents(lstItems, null);
        }

        public static List<Event> LoadEvents(List<IPublishedContent> contents, EventsFilter filter)
        {
            List<Event> items = new List<Event>();
            if (contents != null && contents.Any())
            {
                foreach (var item in contents)
                {
                    //bool includeInList = true;
                    int id = item.Id;
                    string name = string.Empty;

                    string image = string.Empty;
                    var oImage = item.GetPropertyValue<IPublishedContent>("image");
                    if (oImage != null)
                    {
                        image = oImage.Url;
                    }

                    A8.DateFromTo dateFromTo = new A8.DateFromTo();
                    dateFromTo.DateFrom = DateTime.MinValue;
                    dateFromTo.DateTo = DateTime.MaxValue;

                    IPublishedContent dates = item.GetPropertyValue<IPublishedContent>("date");
                    if (dates != null)
                    {
                        DateTime dateFrom = dates.GetPropertyValue<DateTime>("dateFrom").Date;
                        if (dateFrom == null) dateFrom = DateTime.MinValue.Date;
                        DateTime dateTo = dates.GetPropertyValue<DateTime>("dateTo").Date;
                        if (dateTo == null || dateTo == DateTime.MinValue.Date) dateTo = DateTime.MaxValue.Date;
                        dateFromTo.DateFrom = dateFrom;
                        dateFromTo.DateTo = dateTo;
                    }

                    // date is mandatory
                    if (dateFromTo.DateFrom == DateTime.MinValue)
                    {
                        continue;

                        //includeInList = false;
                    }

                    // passed events
                    if (filter != null && !filter.IncludePassed && dateFromTo.DateTo < DateTime.Today)
                    {
                        continue;
                    }

                    string type = item.GetPropertyValue<string>("type");
                    string summary = string.Empty;
                    string location = string.Empty;
                    IEnumerable<IPublishedContent> content = null;
                    string keywords = string.Empty;
                    string description = string.Empty;

                    bool excludeFromList = false;
                    var langsContent = item.GetVortoValue<IPublishedContent>("langs");
                    if (langsContent != null)
                    {
                        excludeFromList = langsContent.GetPropertyValue<bool>("langsEventsExcludeFromList");
                        if (filter != null && excludeFromList && filter.ApplyExcludedFromList)
                        {
                            continue;

                            //includeInList = false;
                        }

                        name = langsContent.GetPropertyValue<string>("langsEventsTitle");
                        summary = langsContent.GetPropertyValue<string>("langsEventsSummary");
                        location = langsContent.GetPropertyValue<string>("langsEventsLocation");
                        content = langsContent.GetPropertyValue<IEnumerable<IPublishedContent>>("langsEventsContent");
                        IPublishedContent oSeo = langsContent.GetPropertyValue<IPublishedContent>("langsEventsSeo");
                        keywords = oSeo.GetPropertyValue<string>("seoKeywords");
                        description = oSeo.GetPropertyValue<string>("seoDescription");
                    }
                    else
                    {
                        continue;

                        //includeInList = false;
                    }
                    if (name.Length == 0) name = item.Name;

                    // 200 is normal
                    int priority = A8.Common.CheckNum(item.GetPropertyValue<nuPickers.Picker>("priority").SavedValue);
                    if (priority == 0) priority = 200;

                    //if (includeInList)
                    {
                        items.Add(new A8.Catalogues.Event(
                            id,
                            name,
                            image,
                            type,
                            dateFromTo,
                            summary,
                            content,
                            location,
                            keywords,
                            description,
                            excludeFromList,
                            priority
                        ));
                    }
                }
            }

            // default sort by priority
            return items?.OrderBy(x => x.Priority).ToList();
        }
    }

    public class EventsFilter
    {
        public string EventType { get; set; }
        public bool IncludePassed { get; set; }
        public bool ApplyExcludedFromList { get; set; }

        public EventsFilter() { }
    }
}