﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace A8
{
    public static class LayoutFunctions
    {
        public static IPublishedContent GetCatalogueLandingPage(string catalogueList, string catalogueType)
        {
            IEnumerable<IPublishedContent> cataloguePages = UmbracoContext.Current.ContentCache.GetByXPath("//cataloguePage");
            return cataloguePages.FirstOrDefault(x =>
                x.IsVisible() &&
                x.GetPropertyValue<bool>("isLanding") &&
                (x.GetPropertyValue<IPublishedContent>("catalogueList")?.HasProperty(catalogueList) ?? false) &&
                ((x.GetPropertyValue<IPublishedContent>("catalogueList")?.GetPropertyValue<string>("catalogueType") ?? string.Empty) == catalogueType));
        }

        public static A8.CatalogueListItem GetCatalogueListItem(IPublishedContent content, int parentId)
        {
            A8.CatalogueListItem retVal = null;
            if (content != null)
            {
                switch (content.DocumentTypeAlias)
                {
                    case "cat_OneNews":
                        {
                            A8.Catalogues.OneNews item = A8.Catalogues.News.LoadOneNews(content);
                            if (item != null)
                            {
                                //string linkMore = string.Empty;
                                //string landingPage = GetCatalogueItemsLandingPage("newsList", parentId);
                                //if (landingPage.Length > 0)
                                //{
                                //    linkMore = landingPage + item.Id + "-" + A8.Common.StringToUrlSafe(item.Name);
                                //}
                                //oItem = new A8.CatalogueListItem(
                                //    item.Id,
                                //    content.DocumentTypeAlias,
                                //    item.Image,
                                //    A8.Common.DateFromToAsString(item.NewsDate, item.NewsDate, true),
                                //    item.Name,
                                //    A8.Common.ShortenWholeWord(A8.Common.Detag(item.Summary), 150),
                                //    linkMore
                                //    );
                            }
                            break;
                        }
                    case "cat_Event":
                        {
                            A8.Catalogues.Event item = A8.Catalogues.Events.LoadEvent(content);
                            if (item != null)
                            {
                                //string linkMore = string.Empty;
                                //string landingPage = GetCatalogueItemsLandingPage("publicAnnouncementsList", parentId);
                                //if (landingPage.Length > 0)
                                //{
                                //    linkMore = landingPage + item.Id + "-" + A8.Common.StringToUrlSafe(item.Name);
                                //}
                                //oItem = new A8.CatalogueListItem(
                                //    item.Id,
                                //    content.DocumentTypeAlias,
                                //    item.Image,
                                //    A8.Common.DateFromToAsString(item.PublicAnnouncementDate, item.PublicAnnouncementDate, true),
                                //    item.Name,
                                //    A8.Common.ShortenWholeWord(A8.Common.Detag(item.Summary), 150),
                                //    linkMore
                                //    );
                            }
                            break;
                        }
                }
            }
            return retVal;
        }
    }
}