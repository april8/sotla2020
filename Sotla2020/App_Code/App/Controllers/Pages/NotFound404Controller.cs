﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace A8
{
    public class NotFound404Controller : PageBaseController
    {
        public override ActionResult Index(RenderModel model)
        {
            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = 404;
            Response.StatusDescription = "Not Found";

            return base.Index(new PageBaseModel(model));
        }
    }
}