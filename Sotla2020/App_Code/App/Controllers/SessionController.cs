﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace A8
{
    public class SessionController : Controller
    {
        public ActionResult Index()
        {
            return this.Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SessionSet(string k, dynamic v)
        {
            Session[k] = v;

            return this.Json(new { value = Session[k] }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SessionGet(string k)
        {
            return this.Json(new { value = JsonConvert.SerializeObject(Session[k]) }, JsonRequestBehavior.AllowGet);
        }
    }
}