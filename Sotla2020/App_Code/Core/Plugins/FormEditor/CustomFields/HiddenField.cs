﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FormEditor.Fields;
using Umbraco.Core.Models;

namespace A8.FormEditor.Fields
{
	public class HiddenField : FieldWithMandatoryValidation
	{
		public HiddenField()
		{
		}

		public override string Type => "a8.custom.hidden";
		public override string PrettyName => "Hidden field (A8)";

		public string UrlParameter {
			get;
			set;
		}
		public string Value {
			get
			{
				if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString[UrlParameter]) && this.HasSubmittedValue == false)
				{
						this.SubmittedValue = HttpContext.Current.Request.QueryString[UrlParameter];
				}
				return this.SubmittedValue;
			}
			set {
			}
		}
	}
}