﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FormEditor.Fields;

namespace A8.FormEditor.Fields
{
	public class TextboxField : FieldWithPlaceholder
	{
		public TextboxField()
		{
		}

		public override string Type => "a8.custom.textbox";
		public override string PrettyName => "Text box field (A8)";

		public int MaxLength { get; set; }

		public string UrlParameter
		{
			get;
			set;
		}
		public string Value
		{
			get
			{
				if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString[UrlParameter]) && this.HasSubmittedValue == false)
				{
					this.SubmittedValue = HttpContext.Current.Request.QueryString[UrlParameter];
				}
				return this.SubmittedValue;
			}
			set
			{
			}
		}

		public bool IsReadOnly
		{
			get;
			set;
		}
	}
}