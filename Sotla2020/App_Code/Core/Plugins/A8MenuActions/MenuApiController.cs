﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

namespace A8
{
    public class MenuApiController : UmbracoApiController
    {
        [HttpPost]
        [Authorize]
        public Hashtable ShowHide(int id)
        {
            Hashtable result = new Hashtable();
            result["valid"] = false;

            var umbracoHelper = new Umbraco.Web.UmbracoHelper(UmbracoContext);
            IPublishedContent content = umbracoHelper.TypedContent(id);
            if (content == null)
            {
                return result;
            }

            IPublishedProperty item = content.GetProperty("hideInMenu");
            if (item == null)
            {
                return result;
            }

            var services = ApplicationContext.Services;
            var menuItem = services.ContentService.GetById(id);

            if ((bool)item.Value)
            {
                menuItem.SetValue("hideInMenu", false);
            }
            else
            {
                menuItem.SetValue("hideInMenu", true);
            }

            services.ContentService.PublishWithStatus(menuItem);

            result["valid"] = true;
            result["path"] = menuItem.Path;

            return result;
        }

        private bool VerifyTrue(string text)
        {
            if (text == null)
            {
                return false;
            }

            if ((text.ToLower() == "true") || (text.ToLower() == "yes") || (text == "1") || (text.ToLower() == "on"))
            {
                return true;
            }

            return false;
        }
    }
}