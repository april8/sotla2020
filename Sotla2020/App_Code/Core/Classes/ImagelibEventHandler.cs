﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Models.EntityBase;
using Umbraco.Core.Services;

namespace A8
{
    public class ImagelibEventHandler : ApplicationEventHandler
    {
        public ImagelibEventHandler()
        {
            MediaService.Deleted += this.MediaServiceDeleted;
            MediaService.Saved += this.MediaServiceSaved;
        }

        private void MediaServiceSaved(IMediaService sender, SaveEventArgs<IMedia> e)
        {
            foreach (var item in e.SavedEntities)
            {
                if (item.ContentType.Alias == "Image")
                {
                    var dirty = (IRememberBeingDirty)item;
                    bool isNew = dirty.WasPropertyDirty("Id");
                    if (!isNew)
                    {
                        A8.Imagelib imagelib = new Imagelib();
                        imagelib.DeleteImageVariations(item);
                    }
                }
            }
        }

        private void MediaServiceDeleted(IMediaService sender, DeleteEventArgs<IMedia> e)
        {
            foreach (var item in e.DeletedEntities)
            {
                if (item.ContentType.Alias == "Image")
                {
                    A8.Imagelib imagelib = new Imagelib();
                    imagelib.DeleteImageVariations(item);
                }
            }
        }
    }
}