﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace A8
{
    public class Global : Umbraco.Web.UmbracoApplication
    {
        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started
            string sessionId = Session.SessionID;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (A8.Common.VerifyTrue(A8.Common.GetSettings("a8PageErrorHandlerEnabled")))
            {
                Exception ex = Server.GetLastError();
                Server.ClearError();
                if (ex.GetType() == typeof(HttpException))
                {
                    HttpException httpEx = (HttpException)ex;
                    if (httpEx.GetHttpCode() == 404)
                    {
                        Response.Redirect("/notfound404");
                        return;
                    }
                }

                string projectName = A8.Common.GetSettings("a8ProjectName");
                projectName = (projectName.Length > 0 ? projectName + ", " : string.Empty) + HttpContext.Current.Request.Url;
                A8.Common.SendToDeveloper("##ERROR## - " + projectName,
                    ex.ToString() +
                    ListServerVariables()
                    );

                Response.TrySkipIisCustomErrors = true;
                Response.StatusCode = 500;
                // azure problem
                //HttpContext.Current.Response.Status = "500 Internal Server Error";
                //HttpContext.Current.Response.AddHeader("Status Code", "500");
                HttpContext.Current.Server.Transfer("~/Error500.html", false);
                HttpContext.Current.Response.End();
            }

        }

        private string ListServerVariables()
        {
            string variables = "<br><br><br>";
            foreach (string var in Request.ServerVariables)
            {
                variables += "<tr><td valign=\"top\" >" + var + "</td><td valign=\"top\" >" + Request[var] + "</td></tr>";
            }
            return "<table border=\"1\">" + variables + "</table>";
        }
    }
}
