﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;

namespace A8
{
    public class CustomRoutingEventHandler : ApplicationEventHandler
    {

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            RegisterCustomRoutes();
        }

        private static void RegisterCustomRoutes()
        {
            RouteTable.Routes.MapRoute(
               name: "SessionController",
               url: "Session/{action}/{id}",
               defaults: new { controller = "Session", action = "Index" });

            RouteTable.Routes.MapRoute(
                name: "Error500Controller",
                url: "Error500/{action}",
                defaults: new { controller = "Error500", action = "Index" });

            RouteTable.Routes.MapRoute(
                name: "FormsPreviewController",
                url: "forms/{*anything}",
                defaults: new { controller = "NoPreview", action = "Index" });

            RouteTable.Routes.MapRoute(
                name: "CataloguePreviewController",
                url: "catalogues/{*anything}",
                defaults: new { controller = "NoPreview", action = "Index" });

            RouteTable.Routes.MapRoute(
                name: "SiteSettingsPreviewController",
                url: "{lang}/site-settings/{*anything}",
                defaults: new { controller = "NoPreview", action = "Index" });

        }

    }
}