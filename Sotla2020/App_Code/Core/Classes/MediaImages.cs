﻿using Our.Umbraco.Vorto.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace A8
{
    /*
     * Example:
     * @{
     *  A8.MediaImage thumbnail = A8.MediaImages.LoadMediaImage(Model.GetPropertyValue<IPublishedContent>("thumbnail"));
     * }
     * 
     * <img src="@(thumbnail.Url)?anchor=center&amp;mode=crop&amp;width=400&amp;height=300" alt="@Html.Encode(thumbnail.Alt)" title="@Html.Encode(thumbnail.Title)" />
     * <img src="@(thumbnail.Url)" alt="@Html.Encode(thumbnail.Alt)" title="@Html.Encode(thumbnail.Title)" />
     * <div>@(thumbnail.Description)</div>
     */

    public class MediaImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Alt { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public MediaImage()
        { }

        public MediaImage(int id, string url, string alt, string title, string description)
        {
            this.Id = id;
            this.Url = url;
            this.Alt = alt;
            this.Title = title;
            this.Description = description;
        }
    }

    public static class MediaImages
    {
        public static MediaImage LoadMediaImage(IPublishedContent content)
        {
            return LoadMediaImage(content, string.Empty, string.Empty, string.Empty);
        }

        public static MediaImage LoadMediaImage(IPublishedContent content, string alt, string title, string description)
        {
            MediaImage result = null;

            if (content != null)
            {
                result = new MediaImage();
                result.Id = content.Id;
                result.Url = content.Url;
                result.Alt = alt;
                result.Title = title;
                result.Description = description;
                IPublishedContent langs = content.GetVortoValue<IPublishedContent>("langs");
                if (langs != null)
                {
                    if (result.Alt == string.Empty)
                    {
                        result.Alt = langs.GetPropertyValue<string>("imageAlt");
                    }
                    if (result.Title == string.Empty)
                    {
                        result.Title = langs.GetPropertyValue<string>("imageTitle");
                    }
                    if (result.Description == string.Empty)
                    {
                        result.Description = langs.GetPropertyValue<string>("imageDescription");
                    }
                }
            }
            else
            {
                result.Id = 0;
                result.Url = A8.Constants.EmptyGif;
                result.Alt = string.Empty;
                result.Title = string.Empty;
                result.Description = string.Empty;
            }
            return result;
        }
    }
}