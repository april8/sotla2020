﻿using DevTrends.MvcDonutCaching;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace A8.Cache
{
    public class PageCache : DonutOutputCacheAttribute, IActionFilter
    {
        private string cacheKeyPrefix = "";
        public string CacheKey
        {
            get
            {
                string cacheKeySuffix = "url=" + HttpContext.Current.Request.Path + GetQueryParams() + "#";
                return this.cacheKeyPrefix + cacheKeySuffix;
            }
        }

        public CacheSettings cacheSettings;

        public static void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            ClearAllCache();
        }
        public static void ContentService_UnPublished(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            ClearAllCache();
        }

        public static bool ClearAllCache()
        {
            DevTrends.MvcDonutCaching.OutputCacheManager cacheManager = new DevTrends.MvcDonutCaching.OutputCacheManager();

            System.Web.Caching.Cache mvcCache = new System.Web.Caching.Cache();
            if (mvcCache["a8-page_cache"] == null)
            { // clear all :)
                cacheManager.RemoveItems();
                mvcCache.Remove("a8-page_cache");
                return true;
            }

            foreach (DictionaryEntry cachedPages in (Hashtable)mvcCache["a8-page_cache"])
            {
                if (cachedPages.Value == null || ((A8.Cache.CacheByPage)cachedPages.Value).PageUrls.Count <= 0)
                    continue;

                foreach (string pageCachedUrl in new List<string>(((A8.Cache.CacheByPage)cachedPages.Value).PageUrls))
                {
                    System.Web.Routing.RouteValueDictionary itemkey = new System.Web.Routing.RouteValueDictionary();
                    itemkey.Add("url", pageCachedUrl);
                    cacheManager.RemoveItem(null, null, itemkey);

                    ((A8.Cache.CacheByPage)cachedPages.Value).PageUrls.Remove(pageCachedUrl);
                }
            }
            mvcCache.Remove("a8-page_cache");
            return true;
        }

        private static void ClearCache(int pageId, System.Web.Caching.Cache mvcCache, DevTrends.MvcDonutCaching.OutputCacheManager cacheManager)
        {
            if (((Hashtable)mvcCache["a8-page_cache"]).ContainsKey("pageid-" + pageId))
            {
                foreach (string pageCachedUrl in ((A8.Cache.CacheByPage)((Hashtable)mvcCache["a8-page_cache"])["pageid-" + pageId]).PageUrls)
                {
                    System.Web.Routing.RouteValueDictionary itemkey = new System.Web.Routing.RouteValueDictionary();
                    itemkey.Add("url", pageCachedUrl);
                    cacheManager.RemoveItem(null, null, itemkey);
                }
                ((Hashtable)mvcCache["a8-page_cache"]).Remove("pageid-" + pageId);
            }
        }

        private string GetQueryParams()
        {
            string cacheQueryParams = null;

            if (cacheQueryParams != null)
            {
                return cacheQueryParams;
            }

            cacheQueryParams = "";

            if (!string.IsNullOrEmpty(cacheSettings.VaryByParam) && cacheSettings.VaryByParam.ToLowerInvariant() != "none")
            {
                if (cacheSettings.VaryByParam == "*")
                {
                    cacheQueryParams += HttpContext.Current.Request.Url.Query;
                }
                else
                {
                    var parameters = cacheSettings.VaryByParam.ToLowerInvariant().Split(new[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var parameter in parameters)
                    {
                        if (A8.Common.RequestAny(parameter) == "")
                        {
                            continue;
                        }
                        cacheQueryParams += (cacheQueryParams.Contains("?") ? "&" : "?") + parameter + "=" + A8.Common.RequestAny(parameter);
                    }
                }
            }
            return cacheQueryParams;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            cacheSettings = this.BuildCacheSettings();

            this.cacheKeyPrefix = new KeyBuilder().CacheKeyPrefix;

            A8CacheItem cachedItem = null;

            OutputCacheManager ocm = new OutputCacheManager();
            DonutHoleFiller dhf = new DonutHoleFiller(new ActionSettingsSerialiser());

            cachedItem = (A8CacheItem)ocm.GetItem(CacheKey);
            if (cachedItem != null)
            {
                // We inject the previous result into the MVC pipeline
                // The MVC action won't execute as we injected the previous cached result.
                filterContext.Result = new A8ContentResult
                {
                    Content = dhf.ReplaceDonutHoleContent(cachedItem.Content, filterContext, OutputCacheOptions.IgnoreFormData),
                    ContentType = cachedItem.ContentType,
                    DateUpdated = cachedItem.DateUpdated
                };
            }

            var cachingWriter = new StringWriter(CultureInfo.InvariantCulture);
            var originalWriter = filterContext.HttpContext.Response.Output;
            filterContext.HttpContext.Response.Output = cachingWriter;

            // Will be called back by OnResultExecuted -> ExecuteCallback
            filterContext.HttpContext.Items[CacheKey] = new Action<bool>(hasErrors =>
            {
                // Removing this executing action from the context
                filterContext.HttpContext.Items.Remove(CacheKey);

                // We restore the original writer for response
                filterContext.HttpContext.Response.Output = originalWriter;
                if (hasErrors)
                {
                    base.OnActionExecuting(filterContext);
                    return; // Something went wrong, we are not going to cache something bad
                }

                //if HTML string contains <!--NOCACHE-->, page will not be cached
                bool noCacheComment = false;
                if (cachingWriter.ToString().Contains("<!--NOCACHE-->"))
                    noCacheComment = true;

                int pageId = A8.Common.CheckNum(filterContext.Controller.ViewData["cache_page_id-" + filterContext.HttpContext.Request.Path]);
                filterContext.Controller.ViewData.Remove("cache_page_id-" + filterContext.HttpContext.Request.Path);
                if (pageId > 0 && !noCacheComment)
                {
                    // .NET cache store
                    System.Web.Caching.Cache mvcCache = new System.Web.Caching.Cache();
                    if (mvcCache["a8-page_cache"] == null)
                    {
                        mvcCache.Add("a8-page_cache", new Hashtable(), null, DateTime.UtcNow.AddSeconds(cacheSettings.Duration), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.High, null);
                    }

                    // A8 stored cache
                    if (((Hashtable)mvcCache["a8-page_cache"])["pageid-" + pageId] == null)
                    {
                        ((Hashtable)mvcCache["a8-page_cache"])["pageid-" + pageId] = new CacheByPage(pageId, cacheSettings.RelatedDocuments);
                    }
                    if (!((CacheByPage)((Hashtable)mvcCache["a8-page_cache"])["pageid-" + pageId]).PageUrls.Contains(filterContext.HttpContext.Request.Path + GetQueryParams()))
                    {
                        ((CacheByPage)((Hashtable)mvcCache["a8-page_cache"])["pageid-" + pageId]).PageUrls.Add(filterContext.HttpContext.Request.Path + GetQueryParams());
                    }
                }

                // Now we use owned caching writer to actually store data
                var cacheItem = new DevTrends.MvcDonutCaching.A8CacheItem
                {
                    Content = cachingWriter.ToString(),
                    ContentType = filterContext.HttpContext.Response.ContentType
                };

                filterContext.HttpContext.Response.Write(
                    dhf.RemoveDonutHoleWrappers(cacheItem.Content, filterContext, OutputCacheOptions.IgnoreFormData)
                );

                if (pageId > 0 &&
                        !noCacheComment &&
                        cacheSettings.IsServerCachingEnabled &&
                        !Umbraco.Web.UmbracoContext.Current.InPreviewMode &&
                        filterContext.HttpContext.Response.StatusCode == 200
                    )
                {
                    filterContext.Controller.ViewData["currentPage"] = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current).TypedContent(pageId);
                    cacheItem.DateUpdated = ((IPublishedContent)filterContext.Controller.ViewData["currentPage"]).UpdateDate;
                    ocm.AddItem(CacheKey, cacheItem, DateTime.UtcNow.AddSeconds(cacheSettings.Duration));
                }
            });
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (!ExecuteCallback(filterContext, filterContext.Exception != null) || !cacheSettings.IsServerCachingEnabled)
            {
                return;
            }

            // If we are in the context of a child action, the main action is responsible for setting
            // the right HTTP Cache headers for the final response.
            if (!filterContext.IsChildAction)
            {
                if (Umbraco.Web.UmbracoContext.Current.InPreviewMode || filterContext.HttpContext.Response.StatusCode != 200)
                {
                    filterContext.HttpContext.Response.Cache.SetNoStore();
                }
                else if (filterContext.Result.GetType() == typeof(System.Web.Mvc.ViewResult))
                { // if here, page is NOT cached yet
                    filterContext.HttpContext.Response.AddHeader("is-cached", "true");
                    if (((System.Web.Mvc.ViewResult)filterContext.Result).ViewData["currentPage"] != null)
                    {
                        filterContext.HttpContext.Response.Cache.SetLastModified(((IPublishedContent)((System.Web.Mvc.ViewResult)filterContext.Result).ViewData["currentPage"]).UpdateDate);
                    }
                }
                else if (filterContext.Result.GetType() == typeof(A8ContentResult))
                { // if here, page IS cached
                    if (filterContext.HttpContext.Request.Headers.Get("If-Modified-Since") != null)
                    { // Cached on server and in browser
                        if (((A8ContentResult)filterContext.Result).DateUpdated > DateTime.Parse(filterContext.HttpContext.Request.Headers.Get("If-Modified-Since")))
                        {
                            filterContext.HttpContext.Response.AddHeader("is-cached", "true");
                            filterContext.HttpContext.Response.Cache.SetLastModified(((A8ContentResult)filterContext.Result).DateUpdated);
                        }
                        else
                        {
                            filterContext.HttpContext.Response.StatusCode = 304;
                        }
                    }
                    else
                    { // Cached on server but not in browser, return with headers as new
                        filterContext.HttpContext.Response.AddHeader("is-cached", "true");
                        filterContext.HttpContext.Response.Cache.SetLastModified(((A8ContentResult)filterContext.Result).DateUpdated);
                    }
                }

                if (cacheSettings.NoStore)
                {
                    filterContext.HttpContext.Response.Cache.SetNoStore();
                }
            }

        }

        private bool ExecuteCallback(ControllerContext context, bool hasErrors)
        {
            if (string.IsNullOrEmpty(CacheKey))
                return false;

            var callback = context.HttpContext.Items[CacheKey] as Action<bool>;
            if (callback == null)
                return false;

            callback.Invoke(hasErrors);
            return !hasErrors;
        }

    }

    public class CacheByPage
    {
        public int PageId;
        public DateTime Date;
        public List<string> PageUrls = new List<string>();

        public string RelatedDocuments = "";

        public CacheByPage(int pageId)
        {
            this.PageId = pageId;
            this.Date = DateTime.Now;
        }

        public CacheByPage(int pageId, string relatedDocuments)
        {
            this.PageId = pageId;
            this.Date = DateTime.Now;
            this.RelatedDocuments = relatedDocuments;
        }
    }

    // A8 ContentResult Model extended
    public class A8ContentResult : ContentResult
    {
        public DateTime DateUpdated;
    }

}

// A8 CacheItem Model extended
namespace DevTrends.MvcDonutCaching
{
    public class A8CacheItem : CacheItem
    {
        public DateTime DateUpdated;
    }
}

