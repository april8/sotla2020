﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using JavaScriptEngineSwitcher.Core;

using JavaScriptEngineSwitcher.V8;


namespace A8
{
    public class JsEngineSwitcherConfig
    {
        public static void Configure(IJsEngineSwitcher engineSwitcher)
        {
            engineSwitcher.EngineFactories
                .AddV8()
                ;

            engineSwitcher.DefaultEngineName = V8JsEngine.EngineName;
        }
    }
}