﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Threading;
using Our.Umbraco.Vorto.Extensions;
using System.Web.Routing;
using System.Web.Mvc;
using System;
using System.Text.RegularExpressions;
using Umbraco.Core.Persistence;

namespace A8
{
    public static class CommonUmbraco
    {
        public static string CheckPropertyValueString(this IPublishedContent page, string propertyName)
        {
            string result = string.Empty;
            if (page.HasProperty(propertyName) && page.GetPropertyValue(propertyName) != null)
            {
                result = page.GetPropertyValue(propertyName).ToString();
            }
            return result;
        }

        public static string GetImageUrl(IPublishedContent o)
        {
            string retVal = string.Empty;
            if (o != null)
            {
                retVal = o.Url;
            }
            return retVal;
        }

        public static string CurrentLanguageCodeISO()
        {
            IPublishedContent langNode = GetFirstPageContent();
            return langNode.GetCulture().TwoLetterISOLanguageName;
        }

        public static string CurrentLanguageCode()
        {
            IPublishedContent langNode = GetFirstPageContent();
            return langNode.GetCulture().TwoLetterISOLanguageName.ToLower();
        }

        public static IPublishedContent GetContentFromId(int pageId)
        {
            UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            return umbracoHelper.TypedContent(pageId);
        }

        public static IPublishedContent GetMediaFromId(int mediaId)
        {
            UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            IPublishedContent retVal = umbracoHelper.TypedMedia(mediaId);
            return retVal;
        }

        public static IPublishedContent GetCurrentContent()
        {
            return GetCurrentContent(0);
        }
        public static IPublishedContent GetCurrentContent(int pageId)
        {
            UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            if (pageId > 0)
            {
                return GetContentFromId(pageId);
            }
            else
            {
                return umbracoHelper.AssignedContentItem;
            }
        }

        public static IPublishedContent GetFirstPageContent()
        {
            return GetFirstPageContent(0);
        }
        public static IPublishedContent GetFirstPageContent(int pageId)
        {
            return GetCurrentContent(pageId).AncestorOrSelf("firstPage");
        }

        public static IPublishedContent GetSiteSettingsContent()
        {
            return GetSiteSettingsContent(0);
        }
        public static IPublishedContent GetSiteSettingsContent(int pageId)
        {
            return GetFirstPageContent(pageId).FirstChild("siteSettings");
        }

        public static string CurrentLanguageNativeName()
        {
            IPublishedContent langNode = GetFirstPageContent();
            return (langNode.GetCulture().NativeName);
        }

        public enum RootNiveau
        {
            Site,
            Language
        }
        public static IPublishedContent GetFirstInstanceOfDocumentType(RootNiveau rootNiveau, string documentType)
        {
            IPublishedContent retVal = null;
            switch (rootNiveau)
            {
                case RootNiveau.Site:
                    {
                        retVal = GetCurrentContent().AncestorOrSelf("site").FirstChild(documentType);
                        break;
                    }
                case RootNiveau.Language:
                    {
                        retVal = GetFirstPageContent().FirstChild(documentType);
                        break;
                    }
            }
            return retVal;
        }

        /// <summary>
        /// returns 'https://www.site.com/'
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static string CurrentSiteUrl()
        {
            IPublishedContent siteNode = GetCurrentContent().AncestorOrSelf("site");
            string retVal = siteNode.UrlAbsolute();
            if (!retVal.EndsWith("/"))
            {
                retVal += "/";
            }
            return retVal;
        }

        /// <summary>
        /// returns 'https://www.site.com/si/'
        /// </summary>
        /// <returns></returns>
        public static string CurrentSiteLangUrl()
        {
            IPublishedContent siteNode = GetFirstPageContent();
            string retVal = siteNode.UrlAbsolute();
            if (!retVal.EndsWith("/"))
            {
                retVal += "/";
            }
            return retVal;
        }

        /// <summary>
        /// returns 'https://www.site.com/company/about-us/'
        /// </summary>
        /// <returns></returns>
        public static string CurrentSitePageUrl()
        {
            string retVal = GetCurrentContent().UrlAbsolute();
            if (!retVal.EndsWith("/"))
            {
                retVal += "/";
            }
            return retVal;
        }

        public static string FormatOgImagePath(string image)
        {
            // todo: convert to imagelib
            string retVal = string.Empty;
            if (image.Length > 0)
            {
                retVal = A8.CommonUmbraco.CurrentSiteUrl() + string.Join("/", (image + "?anchor=center&mode=crop&width=1200&height=630").Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries));
            }
            return retVal;
        }

        // TODO - add overload for different languages
        public static string Translate(string keyword)
        {
            if (!DictionaryHelper.Service.KeyExist(keyword))
                return keyword;
            string translation = DictionaryHelper.Service.GetValueByKeyAndCulture(keyword, Thread.CurrentThread.CurrentCulture.Name);
            return translation != null && translation != "" ? translation : keyword;
        }

        public static string Translate(string keyword, string lang)
        {
            if (!DictionaryHelper.Service.KeyExist(keyword))
                return keyword;
            string translation = DictionaryHelper.Service.GetValueByKeyAndCulture(keyword, lang);
            return translation != null && translation != "" ? translation : keyword;
        }

        public static string TranslateWithDefault(string keyword, string defaultString)
        {
            string retVal = Translate(keyword);
            if (retVal.Length == 0 || retVal == keyword && defaultString.Length > 0)
            {
                retVal = defaultString;
            }
            return retVal;
        }

        public static int LanguageIdFromNodeId(int nodeId)
        {
            int retVal = 0;
            if (nodeId > 0)
            {
                var umbHelper = new UmbracoHelper(UmbracoContext.Current);
                IPublishedContent content = umbHelper.TypedContent(nodeId);
                if (content != null)
                {
                    var umbLocalizationService = ApplicationContext.Current.Services.LocalizationService;
                    retVal = Common.CheckNum(umbLocalizationService.GetLanguageByIsoCode(content.GetCulture().TwoLetterISOLanguageName).Id);
                }
            }
            return retVal;
        }

        public static string LanguageCodeFromNodeId(int nodeId)
        {
            string retVal = string.Empty;
            if (nodeId > 0)
            {
                var umbHelper = new UmbracoHelper(UmbracoContext.Current);
                IPublishedContent content = umbHelper.TypedContent(nodeId);
                if (content != null)
                {
                    var umbLocalizationService = ApplicationContext.Current.Services.LocalizationService;
                    retVal = Common.CheckStr(umbLocalizationService.GetLanguageByIsoCode(content.GetCulture().TwoLetterISOLanguageName));
                }
            }
            return retVal;
        }

        public static int GetCatalogueItemId(HttpRequest request, string pageUrl)
        {
            string requestedUrl = request.Url.AbsoluteUri;

            // skip preview
            if (requestedUrl.EndsWith(".aspx"))
                requestedUrl = pageUrl;

            string pageQuery = A8.Common.CheckStr(request.Url.Query);
            if (pageQuery.Length > 0)
            {
                requestedUrl = requestedUrl.Replace(pageQuery, string.Empty);
            }

            if (!requestedUrl.EndsWith("/")) requestedUrl += "/";

            requestedUrl = requestedUrl.Replace(pageUrl, string.Empty).Trim();

            if (requestedUrl.Length > 0)
            {
                int itemId = A8.Common.CheckNum(requestedUrl.Split(new char[] { '-' })[0]);
                if (itemId > 0)
                {
                    return itemId;
                }
            }

            return 0;
        }

        //	// relations between nodes and languages
        //	// one site is master, all copied nodes or manualy added relations are child relations 
        public static int MasterLanguageNode(int nodeId, ref IEnumerable<IRelation> masterChildren)
        {
            var relationService = ApplicationContext.Current.Services.RelationService;
            int pageId = 0;

            List<int> children = new List<int>();

            var getParentOrChildTmp = relationService.GetByParentOrChildId(nodeId);

            if (getParentOrChildTmp != null)
            {
                foreach (var relation in getParentOrChildTmp.ToList())
                {
                    if (nodeId == relation.ParentId) // we're already at the "master" language so find childs
                    {
                        masterChildren = relationService.GetByParentOrChildId(nodeId);
                        return nodeId;
                    }
                    else if (nodeId == relation.ChildId) // we're at a child so need to go up first to find the "master"
                    {
                        // if you want cross relations set here 'relation.ParentId', if you want exact relations set here 'nodeid'
                        masterChildren = relationService.GetByParentOrChildId(relation.ParentId);
                        return relation.ParentId;
                    }
                }
            }
            return pageId;
        }

        //	// relations between nodes and languages
        //	// if we have master - get all related children
        public static Dictionary<int, int> ChildrenLanguageNodes(int nodeId, IEnumerable<IRelation> relations)
        {
            var children = new List<int>();

            // also add the master
            children.Add(nodeId);

            foreach (var relation in relations)
            {
                if (nodeId == relation.ParentId)
                {
                    children.Add(relation.ChildId);
                }
            }

            // match nodes with languages and return result
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            foreach (int child in children)
            {
                int langId = LanguageIdFromNodeId(child);
                if (langId != 0 && !dictionary.ContainsKey(langId))
                {
                    dictionary.Add(langId, child);
                }
            }
            return dictionary;
        }

        public static List<UrlsAndLanguages> UrlsAndLanguages(IPublishedContent modelContent)
        {
            UmbracoHelper umbHelper = new UmbracoHelper(UmbracoContext.Current);
            //var relationService = ApplicationContext.Current.Services.RelationService;

            var dataList = new List<UrlsAndLanguages>();

            IEnumerable<IRelation> masterchildren = new List<Umbraco.Core.Models.Relation>();
            int masternodeid = MasterLanguageNode(modelContent.Id, ref masterchildren);
            int pageid;
            int nodelang = LanguageIdFromNodeId(modelContent.Id);
            var url_lang = new Dictionary<string, string>();

            IEnumerable<IPublishedContent> langNodes = modelContent.Site().Children("firstPage");
            foreach (var langNode in langNodes)
            {
                UrlsAndLanguages data = new UrlsAndLanguages();
                data.NativeName = langNode.GetCulture().NativeName;
                data.LangCode = langNode.GetCulture().TwoLetterISOLanguageName;
                var langId = LanguageIdFromNodeId(langNode.Id);
                if (nodelang == langId)
                {
                    data.IsCurrentLanguage = true;
                }

                if (ChildrenLanguageNodes(masternodeid, masterchildren).TryGetValue(langId, out pageid))
                {
                    data.Url = umbHelper.UrlAbsolute(pageid);
                }
                else
                {
                    //data.Url = CurrentSiteUrl() + (data.LangCode == "sl" ? "si" : data.LangCode) + "/";
                    data.Url = CurrentSiteUrl() + data.LangCode + "/";
                    data.NoRelation = true;
                }

                if (data.Url != null)
                {
                    dataList.Add(data);
                }
            }
            return dataList;
        }

        public static void Call404Page(IPublishedContent page)
        {
            var errorPage = page.AncestorsOrSelf("firstPage").DescendantsOrSelf("notFound404").FirstOrDefault();
            if (errorPage != null)
            {
                HttpContext.Current.Response.Redirect(errorPage.Url);
            }
            else
            {
                UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                errorPage = umbracoHelper.TypedContentAtRoot().FirstOrDefault().DescendantOrSelf("notFound404");
                if (errorPage != null)
                {
                    HttpContext.Current.Response.Redirect(errorPage.Url);
                }
            }
            throw new HttpException(404, "Not found");
        }

        #region Link
        public static string GenerateLink(Newtonsoft.Json.Linq.JObject linkData)
        {
            return GenerateLink(linkData, "", false);
        }
        public static string GenerateLink(Newtonsoft.Json.Linq.JObject linkData, string forceTarget)
        {
            return GenerateLink(linkData, forceTarget, false);
        }
        public static string GenerateLink(Newtonsoft.Json.Linq.JObject linkData, string forceTarget, bool onlyHref)
        {
            string link = string.Empty;

            if (linkData == null)
            {
                return link;
            }

            var target = Common.CheckStr(linkData.GetValue("target"));
            if (target.Length == 0)
            {
                target = "_self";
            }
            if (forceTarget.Length > 0)
            {
                target = forceTarget;
            }

            string querystring = Common.CheckStr(linkData.GetValue("querystring"));
            if (querystring.Length > 0)
            {
                if (!querystring.StartsWith("?"))
                {
                    querystring = "?" + querystring;
                }
            }

            string url = Common.CheckStr(linkData.GetValue("url"));
            int id = Common.CheckNum(linkData.GetValue("id"));

            link = (url.Length > 0 ? url : "#");
            if (id > 0)
            {
                link = umbraco.library.NiceUrl(id);
            }

            if (onlyHref)
            {
                link = link + querystring;
            }
            else
            {
                link = "href=\"" + link + querystring + "\" target=\"" + target + "\"";
            }
            return link;
        }

        public static string GenerateLinkTitle(Newtonsoft.Json.Linq.JObject linkData)
        {
            return GenerateLinkTitle(linkData, false);
        }

        public static string GenerateLinkTitle(Newtonsoft.Json.Linq.JObject linkData, bool fallbackTitle)
        {
            string title = string.Empty;

            if (linkData == null)
            {
                return title;
            }

            title = Common.CheckStr(linkData.GetValue("name"));
            if (title.Length == 0 && fallbackTitle)
            {
                title = Common.CheckStr(linkData.GetValue("url"));
                int id = Common.CheckNum(linkData.GetValue("id"));
                if (id > 0)
                {
                    title = umbraco.library.NiceUrl(id);
                }
            }

            return title;
        }
        #endregion

        public static Dictionary<string, string> Redirects()
        {
            return (Dictionary<string, string>)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem("allpageredirects", () =>
            {
                var dict = new Dictionary<string, string>();
                using (var fs = System.IO.File.OpenRead(HostingEnvironment.MapPath("~/config/redirectPairs.csv")))
                using (var reader = new StreamReader(fs))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (!string.IsNullOrEmpty(line) && !line.StartsWith("//"))
                        {
                            var values = line.Split(',');
                            string key = Common.StripHttpProtocol(Common.CheckStr(values[0]).Trim());
                            string value = Common.StripHttpProtocol(Common.CheckStr(values[1]).Trim());
                            if (!dict.ContainsKey(key))
                            {
                                dict.Add(key, value);
                            }
                        }
                    }
                }
                return dict;
            });
        }

        public static string GetUrlExtensionName(IPublishedContent content, string lang, string vortoProperty, string vortoField)
        {
            string retVal = string.Empty;
            if (content != null && lang.Length > 0 && vortoProperty.Length > 0 && vortoField.Length > 0)
            {
                var langsContent = content.GetVortoValue<IPublishedContent>(vortoProperty, lang);
                if (langsContent != null)
                {
                    retVal = A8.Common.CheckStr(langsContent.GetPropertyValue<string>(vortoField));
                }
            }
            return retVal;
        }

        public static T CreateController<T>(RouteData routeData = null) where T : Controller, new()
        {
            // create a disconnected controller instance
            T controller = new T();

            // get context wrapper from HttpContext if available
            HttpContextBase wrapper;
            if (System.Web.HttpContext.Current != null)
            {
                wrapper = new HttpContextWrapper(System.Web.HttpContext.Current);
            }
            else
            {
                throw new InvalidOperationException(
                    "Can't create Controller Context if no " +
                    "active HttpContext instance is available.");
            }

            if (routeData == null)
            {
                routeData = new RouteData();
            }

            // add the controller routing if not existing
            if (!routeData.Values.ContainsKey("controller") && !routeData.Values.ContainsKey("Controller"))
            {
                routeData.Values.Add("controller", controller.GetType().Name.ToLower().Replace("controller", ""));
            }

            controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);
            return controller;
        }

        // ======================================
        // to je custom Search funkcija.
        // Problem pri obstojecem searchu je, da je query razbil na posamezne besede, kar je za iskanje neuporabno.
        // Recimo, da si hotel iskati: "Rez stare trte", je vrnil 100 rezultatov, in to vse node z ali "rez" ali "stare" ali "trte".
        // Ta nova funkcija pa isce po frazi, da isce tudi sredi texta npr: "Obiscite dogodek Rez stare trte v Mariboru."
        // Ne uporablja pa wildcarda (kot originalna, kar je pa tudi v redu). Torej ce isces hot* ne bo nasel hotel-a
        // ======================================
        public static List<IPublishedContent> Search(string phrase)
        {
            UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var searcher = Examine.ExamineManager.Instance.DefaultSearchProvider;
            List<IPublishedContent> resultList = new List<IPublishedContent>();
            if (phrase.Length > 0)
            {
                phrase = "\"" + phrase.Replace("\"", "\\\"") + "\"";
                string luceneQuery = "+" + phrase;
                var criteria = searcher.CreateSearchCriteria().RawQuery(luceneQuery);
                var results = searcher.Search(criteria);
                if (results != null)
                {
                    IPublishedContent tmp = null;
                    foreach (var item in results)
                    {
                        tmp = umbracoHelper.TypedContent(item.Id);
                        if (tmp != null)
                        {
                            resultList.Add(umbracoHelper.TypedContent(item.Id));
                        }
                    }
                }
            }
            return resultList;
        }
        // ======================================

        // get items from dictionary and cache them
        public static List<A8.DictionaryKey> TranslateListCache(string keyword)
        {
            string langCode = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            if (keyword.Length == 0 || langCode.Length == 0) return null;
            string cacheCode = keyword + "|" + langCode + "|dictionary";
            return HttpRuntime.Cache.GetCachedItem(cacheCode, () => _TranslateList(keyword, langCode));
        }

        public static List<A8.DictionaryKey> TranslateList(string keyword, string langCode)
        {
            if (keyword.Length == 0 || langCode.Length == 0) return null;
            string cacheCode = keyword + "|" + langCode + "|dictionary";
            return HttpRuntime.Cache.GetCachedItem(cacheCode, () => _TranslateList(keyword, langCode));
        }

        private static List<A8.DictionaryKey> _TranslateList(string keyword, string langCode)
        {
            // also this is possible to avoid sql
            // var allChannels = DictionaryHelper.Service.GetAll().Where(x => x.Key.StartsWith("dashboard.channels.")).Where(x => x.Culture.ToLower() == Model.GetCulture().TwoLetterISOLanguageName.ToLower());

            if (keyword.Length == 0 || langCode.Length == 0) return null;

            string qry = "" +
                "SELECT " +
                "a.[key] AS [Key], " +
                "COALESCE (NULLIF (CAST(( " +
                "SELECT TOP(1) b.value FROM dbo.cmsLanguageText AS b INNER JOIN dbo.umbracoLanguage AS c ON b.languageId = c.id " +
                "WHERE(b.UniqueId = a.id) AND(c.languageISOCode = N'" + A8.Common.StringToSql(langCode) + "')) AS nvarchar(MAX)), ''), a.[key]) AS Value " +
                "FROM dbo.cmsDictionary AS a LEFT OUTER JOIN " +
                "dbo.cmsDictionary AS b ON a.parent = b.id " +
                "WHERE(b.[key] = N'" + A8.Common.StringToSql(keyword) + "') " +
                "ORDER BY[Key]";

            UmbracoDatabase db = ApplicationContext.Current.DatabaseContext.Database;
            List<A8.DictionaryKey> retVal = db.Fetch<A8.DictionaryKey>(qry);
            return retVal;
        }
        // --------------------------------------

        #region Head elements
        private static Dictionary<string, string> HeadElements = null; // new Dictionary<string, string>();

        public static void AddHeadElements(string key, string value)
        {
            if (value == null || value.Length == 0)
            {
                return;
            }

            if (UmbracoContext.Current.HttpContext.Items["head_elements"] != null)
            {
                HeadElements = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["head_elements"];
            }

            if (HeadElements == null)
            {
                HeadElements = new Dictionary<string, string>();
            }

            if (HeadElements.ContainsKey(key))
            {
                if (!HeadElements[key].Contains(value))
                {
                    HeadElements[key] += " " + value;
                }
            }
            else
            {
                HeadElements.Add(key, value);
            }

            UmbracoContext.Current.HttpContext.Items["head_elements"] = HeadElements;
        }

        public static string GetHeadElements()
        {
            string elements = string.Empty;

            if (UmbracoContext.Current.HttpContext.Items["head_elements"] == null)
            {
                return elements;
            }

            HeadElements = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["head_elements"];

            if (HeadElements == null || HeadElements.Count <= 0)
            {
                return elements;
            }

            foreach (var attr in HeadElements)
            {
                elements += attr.Value + "\n";
            }

            //Clear if we used it
            UmbracoContext.Current.HttpContext.Items["head_elements"] = HeadElements = null;

            return elements;
        }
        #endregion

        #region Body attributes
        private static Dictionary<string, string> BodyAttributes = null;

        public static void AddBodyAttribute(string attribute, string value)
        {
            if (BodyAttributes == null)
            {
                BodyAttributes = new Dictionary<string, string>();
            }

            if (UmbracoContext.Current.HttpContext.Items["body_atributes"] != null)
            {
                BodyAttributes = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["body_atributes"];
            }

            if (BodyAttributes.ContainsKey(attribute))
            {
                if (!BodyAttributes[attribute].Contains(value))
                {
                    BodyAttributes[attribute] += " " + value;
                }
            }
            else
            {
                BodyAttributes.Add(attribute, value);
            }

            UmbracoContext.Current.HttpContext.Items["body_atributes"] = BodyAttributes;
        }

        public static string GetBodyAttributesAsString()
        {
            string attrs = string.Empty;

            if (UmbracoContext.Current.HttpContext.Items["body_atributes"] == null)
            {
                return attrs;
            }

            BodyAttributes = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["body_atributes"];

            if (BodyAttributes == null || BodyAttributes.Count <= 0)
            {
                return attrs;
            }

            foreach (var attr in BodyAttributes)
            {
                attrs += " " + attr.Key + "=\"" + attr.Value + "\"";
            }

            //Clear if we used it
            UmbracoContext.Current.HttpContext.Items["body_atributes"] = BodyAttributes = null;

            return attrs;
        }
        #endregion

    }

    public class GenericController : Controller { }

    public class UrlsAndLanguages
    {
        public UrlsAndLanguages()
        { }

        public bool IsCurrentLanguage { get; set; }
        public string LangCode { get; set; }
        public string Url { get; set; }
        public string NativeName { get; set; }
        public bool NoRelation { get; set; }
    }

}