﻿using System.Linq;
using System.Net.Mail;
using FormEditor;
using FormEditor.Events;
using FormEditor.Fields;
using Umbraco.Core;

namespace A8
{
    public class ApplicationEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            FormModel.BeforeSendMail += FormModelOnBeforeSendMail;
        }

        private void FormModelOnBeforeSendMail(FormModel sender, FormEditorMailCancelEventArgs formEditorMailCancelEventArgs)
        {
            // find the first email field and grab the submitted email address
            var from = sender
                .AllFields()
                .OfType<EmailField>()
                .FirstOrDefault(f => f.HasSubmittedValue)
                ?.SubmittedValue;

            if (string.IsNullOrWhiteSpace(from))
            {
                // no submitted email address
                return;
            }

            // set the from address on the email
            // this completely replaces from field - goes to junck
            //formEditorMailCancelEventArgs.MailMessage.From = new MailAddress(from);

            // this adds both sender and replyto mail to replyto field
            //formEditorMailCancelEventArgs.MailMessage.ReplyToList.Add(new MailAddress(from));

            // onbehalf
            formEditorMailCancelEventArgs.MailMessage.Sender = formEditorMailCancelEventArgs.MailMessage.From;
            formEditorMailCancelEventArgs.MailMessage.From = new MailAddress(from);
            formEditorMailCancelEventArgs.MailMessage.ReplyToList.Add(new MailAddress(from));
        }
    }
}