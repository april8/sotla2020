﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace A8
{
    public class PageBaseController: RenderMvcController
    {
        [A8.Cache.PageCache(Duration = 31536000, VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.ServerAndClient)]
        public override ActionResult Index(RenderModel model)
        {
            Session["dummy"] = DateTime.Now;

            // Cache part 
            var urlPath = ControllerContext.HttpContext.Request.Path;
            ViewData["cache_page_id-" + urlPath] = model.Content.Id;
            // End Cache part

            return base.Index(model);
        }

        public static class GlobalProperties
        {
            #region Head elements
            public static Dictionary<string, string> HeadElements = null; // new Dictionary<string, string>();

            public static void AddHeadElements(string key, string value)
            {
                if (value == null || value == "")
                    return;

                if (UmbracoContext.Current.HttpContext.Items["head_elements"] != null)
                    HeadElements = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["head_elements"];

                if (HeadElements == null)
                    HeadElements = new Dictionary<string, string>();

                if (HeadElements.ContainsKey(key))
                {
                    if (!HeadElements[key].Contains(value))
                        HeadElements[key] += " " + value;
                }
                else
                    HeadElements.Add(key, value);

                UmbracoContext.Current.HttpContext.Items["head_elements"] = HeadElements;
            }

            public static string GetHeadElements()
            {
                string elements = "";
                if (UmbracoContext.Current.HttpContext.Items["head_elements"] == null)
                    return elements;

                HeadElements = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["head_elements"];
                if (HeadElements == null || HeadElements.Count <= 0)
                    return elements;

                foreach (var attr in HeadElements)
                    elements += attr.Value + "\n";

                //Clear if we used it
                UmbracoContext.Current.HttpContext.Items["head_elements"] = HeadElements = null;

                return elements;
            }
            #endregion

            #region Body attributes
            public static Dictionary<string, string> BodyAttributes = null; // new Dictionary<string, string>();

            public static void AddBodyAttribute(string attribute, string value)
            {
                if (BodyAttributes == null)
                    BodyAttributes = new Dictionary<string, string>();

                if (UmbracoContext.Current.HttpContext.Items["body_atributes"] != null)
                    BodyAttributes = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["body_atributes"];

                if (BodyAttributes.ContainsKey(attribute))
                {
                    if (!BodyAttributes[attribute].Contains(value))
                        BodyAttributes[attribute] += " " + value;
                }
                else
                    BodyAttributes.Add(attribute, value);

                UmbracoContext.Current.HttpContext.Items["body_atributes"] = BodyAttributes;
            }

            public static string GetBodyAttributesAsString()
            {
                string attrs = "";
                if (UmbracoContext.Current.HttpContext.Items["body_atributes"] == null)
                    return attrs;

                BodyAttributes = (Dictionary<string, string>)UmbracoContext.Current.HttpContext.Items["body_atributes"];
                if (BodyAttributes == null || BodyAttributes.Count <= 0)
                    return attrs;

                foreach (var attr in BodyAttributes)
                    attrs += " " + attr.Key + "=\"" + attr.Value + "\"";

                //Clear if we used it
                UmbracoContext.Current.HttpContext.Items["body_atributes"] = BodyAttributes = null;

                return attrs;
            }
            #endregion
        }
    }
}