angular.module('umbraco').controller('TableEditorController', function ($scope, tableEditorApi, notificationsService, dialogService) {

	//$scope.model.value = null;

	var emptyCellModel = '{"value": ""}';
	var defaultModel = {
		useFirstRowAsHeader: false,
		useLastRowAsFooter: false,
		tableStyle: null,
		columnStylesSelected: [
			null,
			null
		],
		rowStylesSelected: [
			null,
			null,
			null
		],
		cells: [
			[{ value: "" }, { value: "" }],
			[{ value: "" }, { value: "" }],
			[{ value: "" }, { value: "" }],
		]
	}

	//ini
	$scope.model.value = $scope.model.value || defaultModel;
	$scope.showAdvanced = false;
	$scope.model.config.tableStyles = parseStyleConfig($scope.model.config.tableStyles);
	$scope.model.config.columnStyles = parseStyleConfig($scope.model.config.columnStyles);
	$scope.model.config.rowStyles = parseStyleConfig($scope.model.config.rowStyles);

	$scope.canAddRow = function () {
		if (isNaN(parseInt($scope.model.config.maxRows, 10))) {
			return true;
		}

		return ($scope.model.config.maxRows > $scope.model.value.cells.length);
	}

	$scope.addRow = function ($index) {
		if ($scope.canAddRow()) {
			var newRow = [];

			for (var i = 0; i < getColumnCount(); i++) {
				newRow.push(emptyCellModel);
			}

			$scope.model.value.cells.splice($index + 1, 0, JSON.parse("[" + newRow.join(',') + "]"));
		}
	}

	$scope.canAddColumn = function () {

		if (isNaN(parseInt($scope.model.config.maxColumns, 10))) {
			return true;
		}

		return ($scope.model.config.maxColumns > getColumnCount());
	}

	$scope.addColumn = function ($index) {
		if ($scope.canAddColumn()) {

			//style
			$scope.model.value.columnStylesSelected.splice($index + 1, 0, null);

			//cells
			for (var i in $scope.model.value.cells) {
				$scope.model.value.cells[i].splice($index + 1, 0, JSON.parse(emptyCellModel));
			}
		}
	}

	$scope.canRemoveRow = function () {
		return ($scope.model.value.cells.length > 1);
	}

	$scope.removeRow = function ($index) {
		if ($scope.canRemoveRow()) {
			if (confirm("Are you sure you'd like to remove this row?")) {
				$scope.model.value.cells.splice($index, 1);
			}
		}
	}

	$scope.canRemoveColumn = function () {
		return getColumnCount() > 1;
	}

	$scope.removeColumn = function ($index) {
		if ($scope.canRemoveColumn()) {
			if (confirm("Are you sure you'd like to remove this column?")) {
				$scope.model.value.columnStylesSelected.splice($index, 1);

				for (var i in $scope.model.value.cells) {
					$scope.model.value.cells[i].splice($index, 1);
				}
			}
		}
	}

	$scope.canSort = function () {
		return ($scope.model.value.cells.length > 1);
	}

	//sort config
	$scope.sortableOptions = {
		axis: 'y',
		cursor: "move",
		handle: ".handle",
		update: function (ev, ui) {

		},
		stop: function (ev, ui) {

		}
	};

	$scope.clearTable = function () {
		if (confirm("Are you sure you wish to remove everything from the table?")) {
			$scope.model.value = defaultModel;
		}
	}

	/* Custom */
	$scope.pastedText = function (event) {
		event.preventDefault();
		var clipboardData = (event.originalEvent || event).clipboardData;
		var pastedText = clipboardData.getData('text/html') || clipboardData.getData('text/plain');

		tableEditorApi.postPastedText(pastedText).then(function (response) {
			if (response.data.error == false) {
				$scope.model.value.cells = response.data.table;
				$scope.model.value.columnStylesSelected = new Array(response.data.table[0].length).fill(null);
				$scope.model.value.rowStylesSelected = new Array(response.data.table.length).fill(null);

				notificationsService.success("Import", "Success");
			}
			else {
				notificationsService.error("Import", response.data.error_message);
			}
		});
	}

	$scope.contextClick = function (event) {

		dialogService.open({
			template: '../App_Plugins/TableEditor/tinymice/views/table.editor.tinymice.html',
			show: true,
			dialogData: { text: event.currentTarget.value }, 
			callback: function (tinymiceValue) {
				$(event.currentTarget).val(tinymiceValue);
				setTimeout(function () {
					$(event.currentTarget)[0].dispatchEvent(new Event("input", { bubbles: true }));
				}, 100);
				dialogService.close();
			}
		});
	}
	/* End Custom */

	function getColumnCount() {
		return $scope.model.value.cells[0].length;
	}

	function parseStyleConfig(configString) {
		if (!configString)
			return;

		//Col Style 1,col-style-1

		var lines = configString.split('\n');
		var styles = [{ label: "None", value: null }];

		for (var i in lines) {
			var style = {};
			var temp = lines[i].split(',');

			if (temp[0].trim() != "" && temp[1].trim() != "") {
				style.label = temp[0].trim();
				style.value = temp[1].trim();

				styles.push(style);
			}
		}

		return styles;
	}
}).directive('tableEditorRowControl', function () {

	var linker = function (scope, element, attrs) {

		var $rowControls = jQuery(element).find("td.row-buttons-td div");
		var $rowStyle = jQuery(element).find("td.row-style");
		var selectActive = false;

		$rowStyle.find("select").focus(function () {
			selectActive = true;
		});

		$rowStyle.find("select").blur(function () {
			selectActive = false;
		});

		element.bind('mouseover', function () {
			selectActive = false;
			$rowControls.show();

			element.addClass("row-highlighted");

			if ($rowStyle.find('option').length > 1) {
				$rowStyle.css('visibility', 'visible');
			}
		});

		element.bind('mouseout', function () {
			if (selectActive == false) {
				$rowControls.hide();
				$rowStyle.css('visibility', 'hidden');
				//$rowStyle.find('select').hide();
				element.removeClass("row-highlighted");
			}
		});
	}

	return {
		restrict: "A",
		link: linker
	}
}).directive('tableEditorColumnControl', function () {

	var linker = function (scope, element, attrs) {

		//had to encapsulate all of the jquery in each function due to the dynamic nature of the prop editor

		element.bind('mouseover', function () {
			var $td = jQuery(element);
			var index = $td.index() + 1;
			var $table = $td.closest("table");
			var $tds = $table.find("tbody td:nth-child(" + index + ")");
			var $th1 = $table.find("thead tr:nth-child(1) th:nth-child(" + (index) + ")");
			var $th2 = $table.find("thead tr:nth-child(2) th:nth-child(" + (index) + ")");
			$tds.addClass("col-highlighted");
			$th1.addClass("col-highlighted");
			$th2.addClass("col-highlighted");

			$th1.css('visibility', 'visible');

			if ($th1.find('option').length > 1) {
				$th1.find('select').css('visibility', 'visible');
			}
		});

		element.bind('mouseout', function () {
			var $td = jQuery(element);
			var index = $td.index() + 1;
			var $table = $td.closest("table");
			var $tds = $table.find("tbody td:nth-child(" + index + ")");
			var $th1 = $table.find("thead tr:nth-child(1) th:nth-child(" + index + ")");
			var $th2 = $table.find("thead tr:nth-child(2) th:nth-child(" + (index) + ")");
			$tds.removeClass("col-highlighted");
			$th1.removeClass("col-highlighted");
			$th2.removeClass("col-highlighted");
			$th1.css('visibility', 'hidden');
			$th1.find('select').css('visibility', 'hidden');
		});

		element.on('paste', function (event) {
			scope.pastedText(event);
		});
	}

	return {
		restrict: "A",
		link: linker
	}
}).directive('tableEditorContextClick', function () {

	var linker = function (scope, element, attrs) {

		element.on('contextmenu', function (event) {
			event.preventDefault();
			scope.contextClick(event);
		});
	}

	return {
		restrict: "A",
		link: linker
	}
}).factory("tableEditorApi", function ($http) {
	return {
		postPastedText: function (text) {
			var data = {
				text: text
			}
			return $http.post("backoffice/TableEditor/TableEditorApi/PastedText", getFormUrlEncoded(data), { headers: { "Content-Type": "application/x-www-form-urlencoded" } });
		}
	};

	function getFormUrlEncoded(toConvert) {
		const formBody = [];
		for (const property in toConvert) {
			const encodedKey = encodeURIComponent(property);
			const encodedValue = encodeURIComponent(toConvert[property]);
			formBody.push(encodedKey + '=' + encodedValue);
		}
		return formBody.join('&');
	};
});

