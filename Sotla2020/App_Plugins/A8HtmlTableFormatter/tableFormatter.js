﻿; (function ($) {
    "use strict";
    $.fn.A8TableFormatter = function () {

        //return this.each(function () {

            // this is our parent holder
            var ident = $(this).attr("data-ident");
            var base = $("#a8TableFormatterContent-" + ident);

            // handle paste in cells
            $(document).on('paste', "#a8TableFormatterContent-" + ident, function (e) {
                // disable paste
                e.preventDefault();
            });

            base.contextPopup({
                selector: 'td',
                title: 'Edit table formatter',
                items: [
                    { command: 'addcolbefore', label: 'Add column before' },
                    { command: 'addcolafter', label: 'Add column after' },
                    null, // divider
                    { command: 'removecol', label: 'Remove column' }
                ],
                callback: function (command, o) {
                    var cell = o;
                    var cidx = cell.index();
                    var row = cell.closest("tr");
                    var tbl = cell.closest("tr").closest("table");
                    var tagName;
                    var cells;
                    var t;
                    var r;

                    cell.attr("contenteditable", "");

                    switch (command) {
                        case "addcolbefore":
                            {
                                tbl.find('tr').each(function () {
                                    var z = $(this).find('td,th').eq(cidx);
                                    var zt = z.get(0).tagName;
                                    z.before("<" + zt + " tabindex=1></" + zt + ">");
                                });
                                break;
                            }
                        case "addcolafter":
                            {
                                tbl.find('tr').each(function () {
                                    var z = $(this).find('td,th').eq(cidx);
                                    var zt = z.get(0).tagName;
                                    z.after("<" + zt + " tabindex=1></" + zt + ">");
                                });
                                break;
                            }
                        case "removecol":
                            {
                                if (row.find("td,th").length == 1) {
                                    alert("Last column cannot be removed!");
                                }
                                else {
                                    r = confirm("Confirm removing column!");
                                    if (r) {
                                        tbl.find('tr').each(function () {
                                            $(this).find('td,th').eq(cidx).remove();
                                        });
                                    }
                                }
                                break;
                            }
                    }

                    cell.removeAttr("contenteditable");

                    var e = $.Event("keyup", { keyCode: 27 });
                    base.trigger(e);

                }
            });

        //});
    };
})(jQuery);