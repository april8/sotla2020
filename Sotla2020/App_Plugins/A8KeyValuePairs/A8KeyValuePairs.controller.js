var app = angular.module("umbraco")
    .controller("A8.KeyValuePairsController", function ($scope, $window) {

        // attempt to get unique id for dom elements for work with jqLite (TODO: find umbraco way)
        $scope.timestamp = Math.floor(Date.now());

        $scope.lists = $scope.model.value || [];

        $scope.new_key = $scope.new_value = "";
        $scope.Add = function () {
            if ($scope.new_key === "") {
                return;
            }
            $scope.lists.push({ key: $scope.new_key, value: $scope.new_value });
            $scope.new_key = $scope.new_value = "";
        };

        $scope.Remove = function (index) {
            var key = $scope.lists[index].key;
            var name = $scope.lists[index].value;
            if ($window.confirm("Do you want to remove:" + (key.length > 0 ? " [" + key : "]") + (name.length > 0 ? " " + name : ""))) {
                $scope.lists.splice(index, 1);
            }
        };

        $scope.$on("formSubmitting", function (ev, args) {
            // do something with output
            // filter out with empty key
            $scope.model.value = angular.toJson($scope.lists.filter(function (el) {
                return el.key.length > 0;
            }));
        });
    });

app.requires.push('dndLists');